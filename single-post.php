<?php

$context = Timber::get_context();
$post = Timber::query_post();
$context['post'] = $post;

$templates = array( 'single-post.twig' );
Timber::render( $templates, $context );

