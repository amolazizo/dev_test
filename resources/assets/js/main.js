/* eslint-disable */
import $ from "jquery";


// usage: log('inside coolFunc',this,arguments);
// http://paulirish.com/2009/log-a-lightweight-wrapper-for-consolelog/
window.log = function() {
    log.history = log.history || []; // store logs to an array for reference
    log.history.push(arguments);
    if (this.console) {
        window.console.log(Array.prototype.slice.call(arguments));
    }
};



window.jQuery = window.$ = require("jquery");


let jQuery = $;

console.log($("body"));


//Show all news
if ($('.news-item').length > 2) {
    $('.news-item:gt(2)').hide();
    $('.show-more-news').show();
  }
 
  $('.show-more-news').on('click', function() {
    $('.news-item:gt(2)').toggle();
    $(this).hide()
  });
  

/* fix safari z-index */
$('#main-container').css('-webkit-transform', 'translate3d(0px, 0px, 2px)');

setTimeout(function() {
    $('#main-container').css('-webkit-transform', 'translate3d(0px, 0px, 4px)');
}, 100);

setTimeout(function() {
    $('#main-container').css('-webkit-transform', 'translate3d(0px, 0px, 3px)');
}, 1000);